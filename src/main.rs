// TODO: Maybe use Moedict for any words not found
// TODO: Maybe add english as english meaning?

use anyhow::{bail, Error, Result};
use once_cell::sync::Lazy;
use regex::Regex;
use serde::{Deserialize, Serialize};
use std::{collections::BTreeMap, fmt::Display, iter::zip, str::FromStr};
use zhuyin_pinyin::*;

const DICT: &str = include_str!("../resources/dict-csld.json");
const TBCL: &str = include_str!("../resources/臺灣華語文能力基準詞語表_111-11-14.csv");

#[derive(Serialize, Deserialize, Clone, Debug)]
struct Word {
    heteronyms: Vec<Variant>,
    non_radical_stroke_count: Option<usize>,
    radical: Option<String>,
    stroke_count: Option<usize>,
    title: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
struct Variant {
    alt: Option<String>,
    bopomofo: String,
    definitions: Vec<Definition>,
    id: String,
    pinyin: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
struct Definition {
    def: String,
    example: Option<Vec<String>>,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
struct CsldWord {
    chs: String,
    cht: String,
    pinyin: Vec<String>,
    zhuyin: String,
    translation: Option<Vec<String>>,
}

impl Display for CsldWord {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let pinyin = self.pinyin.join(",");
        let answer = [self.zhuyin.clone(), pinyin].join(",");
        let answer = format!("\"{answer}\"");

        let question = &self.cht;
        let comment = match &self.translation {
            Some(translation) => translation.join("\\n"),
            None => "".to_string(),
        };
        let instructions = "Type the reading in pinyin or zhuyin!";
        let render_as = "Image";
        let line = [question, &answer, &comment, instructions, render_as].join(",");
        write!(f, "{line}")
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
#[serde(untagged)]
enum Band {
    第1級,
    第1S級,
    第2級,
    第2S級,
    第3級,
    第3S級,
    第4級,
    第4S級,
    第5級,
    第6級,
    第7級,
}

impl Band {
    fn new(string: &str) -> Result<Self> {
        match string {
            "第1級" => Ok(Band::第1級),
            "第1*級" => Ok(Band::第1S級),
            "第2級" => Ok(Band::第2級),
            "第2*級" => Ok(Band::第2S級),
            "第3級" => Ok(Band::第3級),
            "第3*級" => Ok(Band::第3S級),
            "第4級" => Ok(Band::第4級),
            "第4*級" => Ok(Band::第4S級),
            "第5級" => Ok(Band::第5級),
            "第6級" => Ok(Band::第6級),
            "第7級" => Ok(Band::第7級),
            _ => bail!("Bad input for enum."),
        }
    }
}

impl Display for Band {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Band::第1級 => write!(f, "第1級"),
            Band::第1S級 => write!(f, "第1*級"),
            Band::第2級 => write!(f, "第2級"),
            Band::第2S級 => write!(f, "第2*級"),
            Band::第3級 => write!(f, "第3級"),
            Band::第3S級 => write!(f, "第3*級"),
            Band::第4級 => write!(f, "第4級"),
            Band::第4S級 => write!(f, "第4*級"),
            Band::第5級 => write!(f, "第5級"),
            Band::第6級 => write!(f, "第6級"),
            Band::第7級 => write!(f, "第7級"),
            // _ => bail!("Bad input for enum."),
        }
    }
}

impl FromStr for Band {
    type Err = Error;

    fn from_str(string: &str) -> Result<Self, Self::Err> {
        match string {
            "第1級" => Ok(Band::第1級),
            "第1*級" => Ok(Band::第1S級),
            "第2級" => Ok(Band::第2級),
            "第2*級" => Ok(Band::第2S級),
            "第3級" => Ok(Band::第3級),
            "第3*級" => Ok(Band::第3S級),
            "第4級" => Ok(Band::第4級),
            "第4*級" => Ok(Band::第4S級),
            "第5級" => Ok(Band::第5級),
            "第6級" => Ok(Band::第6級),
            "第7級" => Ok(Band::第7級),
            _ => bail!("Failed to parse string"),
        }
    }
}

impl TryFrom<usize> for Band {
    type Error = &'static str;
    fn try_from(value: usize) -> Result<Self, &'static str> {
        match value {
            0 => Ok(Band::第1級),
            1 => Ok(Band::第1S級),
            2 => Ok(Band::第2級),
            3 => Ok(Band::第2S級),
            4 => Ok(Band::第3級),
            5 => Ok(Band::第3S級),
            6 => Ok(Band::第4級),
            7 => Ok(Band::第4S級),
            8 => Ok(Band::第5級),
            9 => Ok(Band::第6級),
            10 => Ok(Band::第7級),
            _ => Err("Failed to parse from usize"),
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
struct TBCLWord {
    band: Band,
    hanzi: String,
    zhuyin: String,
    pinyin: String,
    english: String,
}

impl TBCLWord {
    fn new(vec: Vec<&str>) -> Result<Self> {
        let band = Band::new(vec.get(3).unwrap())?;
        let hanzi = vec.get(1).unwrap().replace(char::is_numeric, "");
        let english = vec.get(3).unwrap().replace('\"', "");
        let zhuyin = vec.get(8).unwrap().to_string();
        let pinyin = vec.get(9).unwrap().to_string();

        Ok(TBCLWord {
            band,
            hanzi,
            english,
            zhuyin,
            pinyin,
        })
    }
}

static UNNEEDED: Lazy<Regex> = Lazy::new(|| {
    Regex::new(
        r"(?x)
            (?:<br>)
            |
            (?:\u{f8f8})
            |
            (?:[臺陸][\u{20df}\u{20dd}]) # Enclosing diamond and circle
        ",
    )
    .unwrap()
});

static CIRCLE: Lazy<Regex> = Lazy::new(|| {
    Regex::new(
        r"(?x)
            (\p{Han})[\u{20df}\u{20dd}] # Enclosing diamond and circle
        ",
    )
    .unwrap()
});

static FONT: Lazy<Regex> = Lazy::new(|| {
    Regex::new(
        r"(?x)
            (:?<font[^>]+>)([^<]+)(:?<[^>]+>) # <font face=...>5̣</font>
        ",
    )
    .unwrap()
});

static BAGUA: Lazy<Regex> = Lazy::new(|| {
    Regex::new(
        r"(?x)
            (:?\n<table.*) # HTML table
        ",
    )
    .unwrap()
});

static PARENS: Lazy<Regex> = Lazy::new(|| {
    Regex::new(
        r"(?x)
            ^
            ([^\(]*)   # Before
            \([^\)]+\) # Parentheses and word inside
            (.*)       # After
            $
        ",
    )
    .unwrap()
});

fn get_tbcl() -> Result<Vec<TBCLWord>> {
    let vec: Vec<TBCLWord> = TBCL
        .lines()
        .skip(1)
        .map(str::trim)
        .map(|l| l.split('\t').collect::<Vec<&str>>())
        .collect::<Vec<Vec<&str>>>()
        .iter()
        .map(|v| TBCLWord::new(v.clone()).expect("Failed to create TBCLWord object"))
        .collect();
    Ok(vec)
}

fn tbcl_in_csld<'a>(csld: &'a [CsldWord], word: &str, zhuyin: &str) -> Option<&'a CsldWord> {
    if !zhuyin.is_empty() {
        match csld.iter().find(|c| c.cht == word && c.zhuyin == zhuyin) {
            Some(csld_word) => Some(csld_word),
            None => csld.iter().find(|c| c.cht == word),
        }
    } else {
        csld.iter().find(|c| c.cht == word)
    }
}

fn merge_lists(
    csld: &[CsldWord],
    tbcl: Vec<TBCLWord>,
) -> Result<(Vec<Vec<CsldWord>>, Vec<String>)> {
    let mut merged = vec![
        vec![],
        vec![],
        vec![],
        vec![],
        vec![],
        vec![],
        vec![],
        vec![],
        vec![],
        vec![],
        vec![],
    ];
    let mut not_found = vec![];
    for word in tbcl {
        let mut variant_found = false;
        let hanzi = word.hanzi.trim();
        let zhuyin = word.zhuyin.clone();
        let band_index = word.band.clone() as usize;
        let mut hanzi_variants = vec![hanzi.to_string()];
        for hanzi in hanzi_variants.clone() {
            if hanzi.contains('/') {
                hanzi_variants = hanzi.split('/').map(str::to_string).collect();
                for hanzi_variant in hanzi_variants.clone() {
                    match tbcl_in_csld(csld, &hanzi_variant, &zhuyin) {
                        Some(csld_word) => {
                            merged[band_index].push(csld_word.clone());
                            variant_found = true;
                        }
                        None => continue,
                    }
                }
            }
        }
        if hanzi.contains('(') {
            let remove_parens = hanzi.replace('(', "");
            let remove_parens = remove_parens.replace(')', "");
            let remove_optional = PARENS.replace(hanzi, "$1$2").to_string();
            hanzi_variants.push(remove_parens);
            hanzi_variants.push(remove_optional);
            for hanzi_variant in hanzi_variants {
                match tbcl_in_csld(csld, &hanzi_variant, &zhuyin) {
                    Some(csld_word) => {
                        merged[band_index].push(csld_word.clone());
                        variant_found = true;
                    }
                    None => continue,
                }
            }
            if hanzi.contains('台') {
                hanzi_variants = vec!["臺", "檯", "颱"]
                    .iter()
                    .map(|&t| hanzi.replace('台', t))
                    .collect();
                for hanzi_variant in hanzi_variants {
                    match tbcl_in_csld(csld, &hanzi_variant, &zhuyin) {
                        Some(csld_word) => {
                            merged[band_index].push(csld_word.clone());
                            variant_found = true;
                        }
                        None => {
                            if !variant_found {
                                not_found.push(hanzi_variant.to_owned());
                            }
                        }
                    }
                }
            }
        } else {
            match tbcl_in_csld(csld, hanzi, &zhuyin) {
                Some(csld_word) => merged[band_index].push(csld_word.clone()),
                None => {
                    if !variant_found {
                        let (zhuyins, pinyins) = if word.zhuyin.contains('/') {
                            let zhuyins = word
                                .zhuyin
                                .split('/')
                                .collect::<Vec<_>>()
                                .iter()
                                .map(|z| z.trim().to_string())
                                .collect::<Vec<_>>();

                            if word.pinyin.contains('/') {
                                let mut pinyins = word
                                    .pinyin
                                    .split('/')
                                    .collect::<Vec<_>>()
                                    .iter()
                                    .map(|z| z.trim().to_string())
                                    .collect::<Vec<_>>();
                                for pinyin in pinyins.iter_mut() {
                                    pinyin.retain(|c| !c.is_whitespace());
                                }

                                (zhuyins, pinyins)
                            } else {
                                let mut pinyins = vec![word.pinyin.clone()];
                                for pinyin in pinyins.iter_mut() {
                                    pinyin.retain(|c| !c.is_whitespace());
                                }
                                (zhuyins, pinyins)
                            }
                        } else {
                            let zhuyins = vec![word.zhuyin.clone()];
                            let mut pinyins = vec![word.pinyin.clone()];
                            for pinyin in pinyins.iter_mut() {
                                pinyin.retain(|c| !c.is_whitespace());
                            }
                            (zhuyins, pinyins)
                        };

                        for (zhuyin, pinyin) in zip(zhuyins, pinyins) {
                            let zhuyin = zhuyin.replace(' ', "\u{3000}");
                            let (zhuyin, pinyin_numbers) = parse_full(&zhuyin)?;
                            let pinyin_numbers = pinyin_numbers.replace('\u{3000}', "");
                            let pinyin = vec![pinyin_numbers, pinyin];
                            let new_word = CsldWord {
                                chs: word.hanzi.clone(),
                                cht: word.hanzi.clone(),
                                pinyin,
                                zhuyin,
                                translation: None,
                            };
                            merged[band_index].push(new_word);
                        }
                    }
                }
            }
        }
    }
    Ok((merged, not_found))
}

fn get_csld() -> Result<Vec<CsldWord>> {
    let vec: Vec<Word> = serde_json::from_str(DICT)?;
    let csld: Vec<CsldWord> = vec
        .iter()
        .flat_map(|w| {
            w.heteronyms
                .iter()
                .map(|v| {
                    let simp = match &v.alt {
                        Some(s) => s,
                        None => &w.title,
                    };
                    let pinyin = UNNEEDED
                        .split(&v.pinyin)
                        .collect::<Vec<&str>>()
                        .first()
                        .unwrap()
                        .to_string();

                    let zhuyin = UNNEEDED
                        .split(&v.bopomofo)
                        .collect::<Vec<&str>>()
                        .first()
                        .unwrap()
                        .to_string();

                    let zhuyin = &zhuyin.replace('｜', "ㄧ");
                    let zhuyin = &zhuyin.replace(' ', "\u{3000}");
                    let zhuyin = &zhuyin.replace("ㄅㄚ˙", "˙ㄅㄚ");
                    let pinyin_diacritics = pinyin.replace('-', "");
                    let pinyin_diacritics = pinyin_diacritics.replace(' ', "");
                    let (zhuyin, pinyin_numbers) = parse_full(zhuyin).unwrap();
                    let pinyin_numbers = pinyin_numbers.replace('\u{3000}', "");
                    let pinyin: Vec<String> = vec![pinyin_numbers, pinyin_diacritics];
                    let pinyin: Vec<String> = pinyin.iter().map(|s| s.replace("ü", "v")).collect();
                    let zhuyin = zhuyin.replace('\u{3000}', "");

                    let translation: Vec<String> = v
                        .definitions
                        .iter()
                        .map(|d| {
                            let def = CIRCLE.replace_all(&d.def, "$1: ");
                            let def = FONT.replace_all(&def, "");
                            BAGUA.replace_all(&def, "").to_string()
                        })
                        .collect();
                    CsldWord {
                        chs: simp.to_string(),
                        cht: w.title.to_string(),
                        pinyin,
                        zhuyin,
                        translation: Some(translation),
                    }
                })
                .collect::<Vec<CsldWord>>()
        })
        .collect();
    Ok(csld)
}

fn main() -> Result<()> {
    let csld = get_csld()?;
    let tbcl = get_tbcl()?;

    let (mut merged, not_found) = merge_lists(&csld, tbcl)?;
    for band in merged.iter_mut() {
        band.dedup();
    }

    for (i, band) in merged.into_iter().enumerate() {
        let band_name = TryInto::<Band>::try_into(i).unwrap().to_string();

        // Merge duplicate characters
        let mut band_map = BTreeMap::new();
        for word in band.into_iter() {
            if !band_map.contains_key(&word.cht) {
                band_map.insert(word.cht.clone(), word);
            } else if let Some(found_word) = band_map.get_mut(&word.cht) {
                found_word.pinyin.extend(word.pinyin);
            }
        }

        let tbcl_string = band_map
            .values()
            .map(|w| w.to_string())
            .collect::<Vec<_>>()
            .join("\n");

        let tbcl_string =
            "Question,Answers,Comment,Instructions,Render as\n".to_string() + &tbcl_string;

        let path = format!("/tmp/tbcl_{band_name}.csv");
        std::fs::write(path, &tbcl_string).expect("Failed to write to file.");
    }

    let not_found_pretty = serde_json::to_string_pretty(&not_found)?;
    std::fs::write("/tmp/not_found_pretty.json", not_found_pretty)
        .expect("Failed to write to file.");

    Ok(())
}
#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn parens_remove_inside() {
        let input1 = "一點(兒)";
        let input2 = "沒(有)用";
        let good1 = "一點";
        let good2 = "沒用";
        let answer1 = PARENS.replace(input1, "$1$2");
        let answer2 = PARENS.replace(input2, "$1$2");

        assert_eq!(good1, answer1);
        assert_eq!(good2, answer2);
    }
}
